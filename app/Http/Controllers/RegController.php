<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ModelUser;

class RegController extends Controller
{
    //
    function create() {
        return view('reg');
    }

    function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
        ]);

        $user =  new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->sendEmailVerificationNotification();
        // auth()->login($user);

        return redirect('main')->with('error', 'your email has been registered, please log in');
    }
}
