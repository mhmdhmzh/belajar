<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class MainController extends Controller
{
    //
    function index()
    {
        return view('loginn');
    }

     //START LOGIN SYSTEM

     function checklogin(Request $request)
     {
         $this->validate($request, [
             'email' => 'required|email',
             'password' => 'required|alphaNum|min:3'
         ]);
 
         $user_data = array(
             'email' => $request->get('email'),
             'password' => $request->get('password'),
         );
 
         $user = User::where('email', $user_data['email'])->first();
        //  dd($user);
         if ($user && $user_data['email']==$user->email) {
             if (Auth::attempt($user_data)) {
                 return redirect('main/successlogin');
             } else {
                 return back()->with('error', 'Wrong password!');
             }
         } else {
             return redirect('registerr')->with('error', 'Your email is not registeres, please register');
         }
     }
 
     function successlogin()
     {
         return view('successlogin');
     }
 
     function logout()
     {
         Auth::logout();
         return redirect('main');
     }
 
     //END LOGIN SYSTEM
}
